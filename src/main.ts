import { PGClient } from "./domain/PGClient";
import { config } from "dotenv";
import { Message } from "discord.js";

config({ path: "../.env" });

const client: PGClient = new PGClient(process.env.BOT_TOKEN);

client.on("ready", async (): Promise<void> => {
    const url: string = await client.generateInvite("ADMINISTRATOR");

    console.log(`\nGuilds     ==      ${client.guilds.cache.size}`);
    console.log(`Users:     ==      ${client.users.cache.size}`);
    console.log(`Commands:  ==      ${client.commands.size}`);
    console.log(`Time:      ==      ${client.getTime()}`);
    console.log(`Invite:    ==      ${url}\n`);

    let statuses = [
        //Add more LOOPING
        "Example status",
        "Example status 2"
    ]

    setInterval(function() {
        let status = statuses[Math.floor(Math.random() * statuses.length)];
        client.user?.setPresence({
            status: "online",
            activity: {
                name: status,
                type: "WATCHING" //Change type
            }
        });
    }, 5000);
});

client.on("message", async (message: Message): Promise<void> => {
    if(message.author.bot) return;
    if(message.channel.type !== "text") return;

    let PREFIX = "!"
    
    if(!message.content.startsWith(PREFIX)) return;

    const args: Array<string> = message.content.slice(PREFIX.length).trim().split(/ +/g);
    const cmd: string = args.shift()!.toLowerCase();

    let command: any;

    if (cmd.length === 0) return;

    if (client.commands.has(cmd)) {
        command = client.commands.get(cmd);
    } else if (client.aliases.has(cmd)) {
        command = client.commands.get(client.aliases.get(cmd)!)
    } else return;

    if (command) command.run(message, args)
});