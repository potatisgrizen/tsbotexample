import { Client } from 'discord.js';
import * as moment from 'moment';
import { load } from './CommandHandler';
import { Command } from './Command';

class PGClient extends Client {

    public commands: Map<string, Command>
    public aliases: Map<string, string>

    constructor(token: any) {

        super();
        super.login(token);

        this.aliases = new Map();
        this.commands = this.getCommands();
    }

    public getTime() {
        return moment(Date.now())
        .toString()
        .split(" ")
        .splice(0, 5)
        .join(" ");
    }

    private getCommands(): Map<string, Command> {
        return this.commands = load(this);
    }

}

export { PGClient }
