import { Command } from "../../domain/Command";
import { Message, MessageEmbed } from "discord.js";
import { PGClient } from "../../domain/PGClient";

/**
 * 
 * Example command
 * Change Name to the name you want
 * @author PotatisGrizen
 * 
 */


class Name extends Command {

    constructor(client: PGClient) {
        super(client, {
            name: "", //Name
            description: "", //Description
            usage: "", //Usage Optional
            aliases: [] //Aliases optional
        })
    }

    run(message: Message, args: Array<string>) {
        
    }

}

export = Name;