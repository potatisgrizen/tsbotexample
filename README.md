You are welcome to use this as much as you want.

Remember to install the following dependencys
    "@types/node": "^13.13.2",
    "discord.js": "^12.2.0",
    "dotenv": "^8.2.0",
    "moment": "^2.24.0",
    "typescript": "^3.8.3"
    
Remember also to change the token in .env

Start the bot from the dist directory.